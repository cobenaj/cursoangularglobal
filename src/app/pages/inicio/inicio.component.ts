import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent {
    nombre = "Javier";
    apellido = "Cobeña";
    loadingvisible = false;

    constructor(private ruta:Router){

    }

    visualizarloading (){
      this.loadingvisible=true;
      setTimeout(() => {
        this.loadingvisible=false;
      }, 2000);
    }

    irPaginaTabla(){
this.ruta.navigate(['tabla']);
    }
}
