import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { IDataEmpleado } from 'src/app/interfaces/empleadosInterface';
import { EmpleadoService } from 'src/app/services/empleado.service';

@Component({
  selector: 'app-pagina-tabla',
  templateUrl: './pagina-tabla.component.html',
  styleUrls: ['./pagina-tabla.component.css']
})
export class PaginaTablaComponent  implements OnInit{
  listEmpleado : IDataEmpleado[] =[];
  columTabla : any;

  constructor(private rutas: Router,
              private empleadoService: EmpleadoService){

  }
  ngOnInit(): void {
    this.iniColumnaTabla();
    console.log('Hola Mundo');
    // this.empleadoService.getAllEmployee().subscribe(
    //   (res) => {
    //     console.log(res);
    //     this.listEmpleado=res.data;
    //   },
    //   (error) => {
    //     console.log(error);
    //   }
    // );

    this.empleadoService.getAllEmployee().subscribe(
      {
        next: (datos) => {
          console.log(datos);
          this.listEmpleado = datos.data;
        },
        error: (err) => {
          console.log(err);
        }
      }
    ); 
  }

  iniColumnaTabla(){
    this.columTabla=[
      {
        field: 'id', header: 'ID'
      },
      {
        field: 'name', header: 'Nombre Empleado'
      },
      {
        field: 'salario', header: 'Salario Empleado'
      },
      {
        field: 'edad', header: 'Edad'
      }
    ]
  }

  regresarInicio(){
    this.rutas.navigate(['inicio']);
  }
}
